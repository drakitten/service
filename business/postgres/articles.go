package postgres

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

const (
	insertArticle             = "INSERT INTO articles (title, content, date, author, category, image) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	getArticleById            = "SELECT id, title, content, date, author, category, image FROM articles WHERE id=$1"
	getLastTenCreatedArticles = "SELECT id, image, author, title, date, category FROM articles"
)

type ArticleModel struct {
	Pool *pgxpool.Pool
}

func (m *ArticleModel) Insert(title, content, category, image, username string) (int, error) {
	var id uint64
	err := m.Pool.QueryRow(context.Background(), insertArticle, title, content, time.Now(), username, category, image).Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (m *ArticleModel) Get(id int) (*Article, error) {
	a := &Article{}
	err := m.Pool.QueryRow(context.Background(), getArticleById, id).Scan(&a.ID, &a.Title, &a.Content, &a.Date, &a.Author, &a.Category, &a.Image)
	if err != nil {
		if err.Error() == "No rows in result set" {
			return nil, err
		} else {
			return nil, err
		}
	}
	return a, nil
}

func (m *ArticleModel) Latest() ([]*Article, error) {
	articles := []*Article{}
	rows, err := m.Pool.Query(context.Background(), getLastTenCreatedArticles)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		a := &Article{}
		err = rows.Scan(&a.ID, &a.Image, &a.Author, &a.Title, &a.Date, &a.Category)
		if err != nil {
			return nil, err
		}
		articles = append(articles, a)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return articles, nil
}
