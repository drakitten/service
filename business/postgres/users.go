package postgres

/*
const (
	insert = "INSERT INTO users (name, email, hashed_password, created, active) VALUES ($1, $2, $3, $4, $5)"
	auth   = "SELECT id, hashed_password FROM users WHERE email = $1"
)

type UserModel struct {
	Pool *pgxpool.Pool
}

func (m *UserModel) Insert(name, email, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}
	var id uint64
	err = m.Pool.QueryRow(context.Background(), insert, name, email, hashedPassword, time.Now(), true).Scan(&id)
	if err != nil {
		return err
	}
	return nil
}


func (m *UserModel) Authenticate(email, password string) (int, error) {
	var id int
	var hashedPassword []byte

	err := m.Pool.QueryRow(context.Background(), auth, email).Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, ErrInvalidCredentials
		} else {
			return 0, ErrInvalidCredentials
		}
	}

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	return id, nil
}
*/
