module soft.net/service

go 1.15

require (
	github.com/ardanlabs/conf v1.5.0
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/dimfeld/httptreemux/v5 v5.4.0
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/golangcollege/sessions v1.2.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
)
