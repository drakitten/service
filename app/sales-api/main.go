package main

import (
	"context"
	"expvar"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"soft.net/service/app/sales-api/handlers"
	"syscall"
	"time"

	"github.com/ardanlabs/conf"
	"github.com/pkg/errors"
)

// The version of the project to be able to trace
var build = "develop"

func main() {
	log := log.New(os.Stdout, "SALES: ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	if err := run(log); err != nil {
		log.Println("main: error:", err)
		os.Exit(1)
	}
}

func run(log *log.Logger) error {
	// CONFIGURATION //
	// With the help of ardanlabs/conf we are able to work with ENV vars even in command line (--help)
	// We also can use noprint
	var cfg struct {
		conf.Version
		Web struct {
			APIHost         string        `conf:"default:0.0.0.0:3000"`
			DebugHost       string        `conf:"default:0.0.0.0:4000"`
			ReadTimeout     time.Duration `conf:"default:5s"`
			WriteTimeout    time.Duration `conf:"default:5s"`
			ShutdownTimeout time.Duration `conf:"default:5s"`
		}
		/*Auth struct {
			KeyID          string `conf:"default:54bb2165-71e1-41a6-af3e-7da4a0e1e2c1"`
			PrivateKeyFile string `conf:"default:/Users/Venera/goprojects/service/private.pem"`
			Algorithm      string `conf:"default:RS256"`
		}*/
	}
	// Declaring the current version and desc of project
	cfg.Version.SVN = build
	cfg.Version.Desc = "Copyright information here."

	// Here we are handling errors while parsing arguments from command line
	if err := conf.Parse(os.Args[1:], "SALES", &cfg); err != nil {
		switch err {
		case conf.ErrHelpWanted:
			usage, err := conf.Usage("SALES", &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config usage")
			}
			fmt.Println(usage)
			return nil
		case conf.ErrVersionWanted:
			version, err := conf.VersionString("SALES", &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config version")
			}
			fmt.Println(version)
			return nil
		}
		return errors.Wrap(err, "parsing config")
	}

	// The start of the APP
	// Printing build version and other config to logs
	expvar.NewString("build").Set(build)
	log.Printf("main: Started: Application initializing: version %q", build)
	defer log.Println("main: Completed")
	out, err := conf.String(&cfg)
	if err != nil {
		return errors.Wrap(err, "generating config string")
	}
	log.Printf("main: Config:\n%v\n", out)

	// =========================================================================
	// Initialize authentication support
	/*
		log.Println("main : Started : Initializing authentication support")

		privatePEM, err := ioutil.ReadFile(cfg.Auth.PrivateKeyFile)
		if err != nil {
			return errors.Wrap(err, "reading auth private key")
		}

		privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privatePEM)
		if err != nil {
			return errors.Wrap(err, "parsing auth private key")
		}

		lookup := func(kid string) (*rsa.PublicKey, error) {
			switch kid {
			case cfg.Auth.KeyID:
				return &privateKey.PublicKey, nil
			}
			return nil, fmt.Errorf("no public key found for the specified kid: %s", kid)
		}

		auth, err := auth.New(cfg.Auth.Algorithm, lookup, auth.Keys{cfg.Auth.KeyID: privateKey})
		if err != nil {
			return errors.Wrap(err, "constructing auth")
		}
	*/
	// The start of the Debugging service
	// DefaultServeMux only for debugging
	// there will be /debug/pprof (import net/http/pprof), /debug/vars (import expvar)
	// I could use expvarman in order to trace all the metrics LIVE:
	// expvarman -ports=":4000" -vars="build,requests,goroutines,errors,mem:memstats.Alloc"
	log.Println("main: Initializing debugging support")
	go func() {
		log.Printf("main: Debug Listening %s", cfg.Web.DebugHost)
		if err := http.ListenAndServe(cfg.Web.DebugHost, http.DefaultServeMux); err != nil {
			log.Printf("main: Debug Listener closed: %v", err)
		}
	}()

	// The start of the API Service
	log.Println("main: Initializing API support")
	// Making a channel to listen interrupt signal from the OS
	// Using buffered channel
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	pool, err := pgxpool.Connect(context.Background(), "user=siteAdmin password=qwerty123 host=localhost port=5433 dbname=site sslmode=disable pool_max_conns=10")
	if err != nil {
		log.Fatalf("Unable to connect to database: %v\n", err)
	}
	defer pool.Close()

	api := http.Server{
		Addr:         cfg.Web.APIHost,
		Handler:      handlers.API(build, shutdown, log, pool),
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
	}

	// Making a channel to listen to errors
	// Buffered channel - so goroutine can exit if don't collect an error
	serverErrors := make(chan error, 1)

	go func() {
		log.Printf("Main API listening in %s", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// SHUTDOWN
	// Blocking main and waiting for shutdown.
	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)
	case sig := <-shutdown:
		log.Printf("main: %v Start shutdown", sig)
		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), cfg.Web.ShutdownTimeout)
		defer cancel()
		// Asking listener to shutdown and shed load.
		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}
	return nil
}
