package handlers

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"net/http"
	"os"
	"soft.net/service/business/mid"
	"soft.net/service/business/postgres"
	"soft.net/service/foundation/web"
)

// API constructs an http.Handler with all app routes defined
// We do not use interface types for return (except error)
// Do not pre-decouple *http.Handle->httptreemux.ContextMux!
func API(build string, shutdown chan os.Signal, log *log.Logger, pool *pgxpool.Pool) *web.App {
	//session := sessions.New([]byte("s6Ndh+pPbnzHbS*+9Pk8qGWhTzbpa@ge"))
	//session.Lifetime = 12 * time.Hour

	app := web.NewApp(shutdown, mid.Logger(log), mid.Errors(log), mid.Metrics(), mid.Panics(log))
	check := check{
		log:      log,
		articles: &postgres.ArticleModel{pool},
		//users:	  &postgres.UserModel{pool},
		//session:  session,
	}
	app.Handle(http.MethodGet, "/readiness", check.readiness)

	app.Handle(http.MethodGet, "/article/create", check.createArticleForm)
	app.Handle(http.MethodPost, "/article/create", check.createArticle)
	app.Handle(http.MethodGet, "/article/:id", check.showArticle)
	/*
		app.Handle(http.MethodGet, "/user/signup", check.signupUserForm)
		app.Handle(http.MethodPost, "/user/signup", check.signupUser)
		app.Handle(http.MethodGet, "/user/login", check.loginUserForm)
		app.Handle(http.MethodPost, "/user/login", check.loginUser)
		app.Handle(http.MethodPost, "/user/logout", check.logout)
		check.session.Enable(app)
	*/
	return app
}
