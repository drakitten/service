package handlers

import (
	"context"
	"fmt"
	"github.com/golangcollege/sessions"
	"html/template"
	"log"
	"net/http"
	"soft.net/service/business/forms"
	"soft.net/service/business/postgres"
	"soft.net/service/foundation/web"
	"strconv"
)

type templateData struct {
	CurrentYear     int
	Flash           string
	Form            *forms.Form
	Article         *postgres.Article
	Articles        []*postgres.Article
	IsAuthenticated bool
}

type check struct {
	build    string
	log      *log.Logger
	articles *postgres.ArticleModel
	//users	 *postgres.UserModel
	session *sessions.Session
}

// Highest level of precision by deciding which thing to use in receiver
// Context needed to many things: timeout and etc
// We need to return error

// Take mux and extend it to needed handler -> web.go
// readiness should be handler but it has diff signature, so we are using another
func (c check) readiness(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	/*if n := rand.Intn(100); n%2 == 0 {
		return web.NewRequestError(errors.New("trusted error"), http.StatusBadRequest)
		//panic("forcing panic")
		//return web.NewShutdownError("forcing shutdown")
	}
	*/
	s, err := c.articles.Latest()
	if err != nil {
		return err
	}
	data := &templateData{Articles: s}
	files := []string{
		"./business/ui/html/home.page.tmpl",
		"./business/ui/html/base.layout.tmpl",
		"./business/ui/html/footer.partial.tmpl",
	}
	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	// Pass in the templateData struct when executing the template.
	err = ts.Execute(w, data)
	if err != nil {
		return err
	}
	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) showArticle(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	params := web.Params(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil || id < 1 {
		return err
	}
	println(id)
	s, err := c.articles.Get(id)
	if err != nil {
		return err
	}
	data := &templateData{Article: s}
	files := []string{
		"./business/ui/html/show.page.tmpl",
		"./business/ui/html/base.layout.tmpl",
		"./business/ui/html/footer.partial.tmpl",
	}
	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	err = ts.Execute(w, data)

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) createArticleForm(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	files := []string{
		"./business/ui/html/create.page.tmpl",
		"./business/ui/html/base.layout.tmpl",
		"./business/ui/html/footer.partial.tmpl",
	}
	data := &templateData{Form: forms.New(nil)}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	err = ts.Execute(w, data)

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) createArticle(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}

	form := forms.New(r.PostForm)
	form.Required("title", "content", "category", "image")
	form.MaxLength("title", 100)
	form.PermittedValues("category", "Anime", "Manga", "Other")

	id, err := c.articles.Insert(form.Get("title"), form.Get("content"), form.Get("category"), form.Get("image"), form.Get("username"))
	if err != nil {
		return err
	}

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	http.Redirect(w, r, fmt.Sprintf("/article/%d", id), http.StatusSeeOther)
	return web.Respond(ctx, w, health, statusCode)
}

/*
func (c check) signupUserForm(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	files := []string{
		"./business/ui/html/signup.page.tmpl",
		"./business/ui/html/base.layout.tmpl",
		"./business/ui/html/footer.partial.tmpl",
	}
	data := &templateData{Form: forms.New(nil)}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	err = ts.Execute(w, data)

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) signupUser(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}

	form := forms.New(r.PostForm)
	form.Required("name", "email", "password")
	form.MaxLength("name", 255)
	form.MaxLength("email", 255)
	form.MatchesPattern("email", forms.EmailRX)
	form.MinLength("password", 10)

	err = c.users.Insert(form.Get("name"), form.Get("email"), form.Get("password"))
	if err != nil {
		return err
	}

	c.session.Put(r, "flash", "Your signup was successful. Please log in.")

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) loginUserForm(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	files := []string{
		"./business/ui/html/login.page.tmpl",
		"./business/ui/html/base.layout.tmpl",
		"./business/ui/html/footer.partial.tmpl",
	}
	data := &templateData{Form: forms.New(nil)}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}
	err = ts.Execute(w, data)

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}

func (c check) loginUser(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}
	form := forms.New(r.PostForm)
	println(form.Get("email") + " " + form.Get("password"))
	id, err := c.users.Authenticate(form.Get("email"), form.Get("password"))
	if err != nil {
		return err
	}
	println(c.session)
	println(r.Context().Value("cache"))
	c.session.Put(r, "authenticatedUserID", id)
	c.session.Put(r, "username", form.Get("email"))

	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}
func (c check) isAuthenticated(r *http.Request) bool {
	return c.session.Exists(r, "authenticatedUserID")
}
func (c check) logout(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	c.session.Remove(r, "authenticatedUserID")
	c.session.Remove(r, "username")
	c.session.Put(r, "flash", "You've been logged out successfully!")
	http.Redirect(w, r, "/", http.StatusSeeOther)
	status := "ok"
	statusCode := http.StatusOK

	health := struct {
		Version string `json:"version"`
		Status  string `json:"status"`
	}{
		Version: c.build,
		Status:  status,
	}
	return web.Respond(ctx, w, health, statusCode)
}
*/
