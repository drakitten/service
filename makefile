# I don't know how to use it in Windows, C:/Program Files/Git (?)

SHELL := /bin/bash

run:
    go run app/sales-api/main.go

runa:
    go run app/admin/main.go

tidy:
    go mod tidy
    go mod vendor

test:
    go test -v ./... -count=1
    staticcheck ./...
