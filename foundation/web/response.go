package web

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"net/http"
)

// Respond converts a Go value to JSON and sends it to client
func Respond(ctx context.Context, w http.ResponseWriter, data interface{}, statusCode int) error {
	// Set the status code for the request logger middleware
	// If context is missing this value > graceful shutdown
	v, ok := ctx.Value(KeyValues).(*Values)
	if !ok {
		return NewShutdownError("web value is missing from context")
	}
	v.StatusCode = statusCode

	// If there is nothing to marshal > set status and return
	if statusCode == http.StatusNoContent {
		w.WriteHeader(statusCode)
		return nil
	}

	// Convert response value to JSON
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	// Set content type and headers once we know marshalling has succeeded
	w.Header().Set("Content-Type", "application/json")

	// Write the status code to response
	w.WriteHeader(statusCode)

	// Send the result back to client
	if _, err := w.Write(jsonData); err != nil {
		return err
	}

	return nil
}

// Sends error back to client
func RespondError(ctx context.Context, w http.ResponseWriter, err error) error {
	// If error is *Error type, then trusted output from handler
	if webErr, ok := errors.Cause(err).(*Error); ok {
		er := ErrorResponse{
			Error:  webErr.Err.Error(),
			Fields: webErr.Fields,
		}
		if err := Respond(ctx, w, er, webErr.Status); err != nil {
			return err
		}
		return nil
	}

	// If not, handler will send 500
	er := ErrorResponse{
		Error: http.StatusText(http.StatusInternalServerError),
	}
	if err := Respond(ctx, w, er, http.StatusInternalServerError); err != nil {
		return err
	}

	return nil
}
