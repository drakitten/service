package web

import (
	"context"
	"github.com/dimfeld/httptreemux/v5"
	"github.com/google/uuid"
	"net/http"
	"os"
	"syscall"
	"time"
)

// ctxKey represents the type of value of the context
type ctxKey int

// KeyValues represent how request values are stored
const KeyValues ctxKey = 1

// Values represent state for each request
type Values struct {
	TraceID    string
	Now        time.Time
	StatusCode int
}

// Handler is  a type that handles an http request within a framework
type Handler func(ctx context.Context, w http.ResponseWriter, r *http.Request) error

// Embeding: App is everything a ContextMux is in order to have custom handler in check.go
// App is (handler with ServeHTTP) is entry point into our app and configures context object
type App struct {
	*httptreemux.ContextMux
	shutdown chan os.Signal
	mw       []Middleware
}

//NewApp creates an App value that handle a set of routes for the app
func NewApp(shutdown chan os.Signal, mw ...Middleware) *App {
	app := App{
		httptreemux.NewContextMux(),
		shutdown,
		mw,
	}
	return &app
}

// SignalShutdown is used to gracefully shutdown the app when integrity issue appeared
func (a *App) SignalShutdown() {
	a.shutdown <- syscall.SIGTERM
}

// Overriding handle of httptreemux with new signature
// ServeHTTP -> HTTPTreeMux -> Handler
func (a *App) Handle(method string, path string, handler Handler, mw ...Middleware) {
	// First wrap handler specific middleware around this handler.
	handler = wrapMiddleware(mw, handler)

	// Add the application's general middleware to the handler chain.
	handler = wrapMiddleware(a.mw, handler)

	h := func(w http.ResponseWriter, r *http.Request) {

		// Set the context with the required values to
		// process the request.
		v := Values{
			TraceID: uuid.New().String(),
			Now:     time.Now(),
		}
		ctx := context.WithValue(r.Context(), KeyValues, &v)

		if err := handler(ctx, w, r); err != nil {
			a.SignalShutdown()
			return
		}
	}
	a.ContextMux.Handle(method, path, h)
}
