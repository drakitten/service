package web

import "github.com/pkg/errors"

// FieldError is used to indicate an error with a specific request field
// Data validation errors in a field level
type FieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// ErrorResponse is the form used for API responses from failures from API
type ErrorResponse struct {
	Error  string       `json:"error"`
	Fields []FieldError `json:"fields,omitempty"`
}

// Trusted application errors
// Error is used to pass an error during the request through the app with web specific concept
type Error struct {
	Err    error
	Status int
	Fields []FieldError
}

// Wraps provided error with HTTP status code. It will be used in handlers
func NewRequestError(err error, status int) error {
	return &Error{err, status, nil}
}

// Error implements the error interface and uses default msg of wrapped error. It will be shown in service logs
func (err *Error) Error() string {
	return err.Err.Error()
}

// shutdown is a type used to graceful shutdown
type shutdown struct {
	Message string
}

// returns error that signals shutdown
func NewShutdownError(message string) error {
	return &shutdown{message}
}

// Error implementation of the error interface
func (s *shutdown) Error() string {
	return s.Message
}

func IsShutdown(err error) bool {
	if _, ok := errors.Cause(err).(*shutdown); ok {
		return true
	}
	return false
}
